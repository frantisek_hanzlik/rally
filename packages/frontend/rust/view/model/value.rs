use std::rc::Rc;

use crate::reactive::rx::Rx;

pub enum Value<T> {
	Static(T),
	Dynamic { rx: Rc<Rx<T>> },
	DynamicWithInitial { initial: T, rx: Rc<Rx<T>> },
}

impl<T> From<T> for Value<T> {
	fn from(value: T) -> Self {
		Self::Static(value)
	}
}

impl<T> From<Rc<Rx<T>>> for Value<T> {
	fn from(rx: Rc<Rx<T>>) -> Self {
		Self::Dynamic { rx }
	}
}

impl<T> From<(T, Rc<Rx<T>>)> for Value<T> {
	fn from((initial, rx): (T, Rc<Rx<T>>)) -> Self {
		Self::DynamicWithInitial { initial, rx }
	}
}
